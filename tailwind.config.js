// https://github.com/tailwindcss/tailwindcss/blob/next/stubs/defaultConfig.stub.js
import defaultConfig from 'tailwindcss/defaultConfig'

export default {
  theme: {
    extend: {
      colors: {
        // main: {
        //   wtf: 'red'
        // },
        'transparent': 'transparent',
        'current': 'currentColor',
        'black': 'hsl(0, 0%, 10%)', // #1A1A1A
        'white': '#ffffff',
        'grey-darkest': 'hsl(0, 0%, 33%)', // #545454
        'grey-darker': 'hsl(0, 0%, 42%)', // #6B6B6B
        'grey-dark': 'hsl(0, 0%, 66%)', // #A8A8A8
        'grey': 'hsl(0, 0%, 82%)', // #D1D1D1
        'grey-light': 'hsl(0, 0%, 86%)', // #DBDBDB
        'grey-lighter': 'hsl(0, 0%, 91%)', // #E8E8E8
        'grey-lightest': 'hsl(0, 0%, 96%)', // #F5F5F5
      },
      fontFamily: {
        'main': ['inter-ui'],
        'heading': ['media-sans-ext'],
        'test': ['test'],
      },
      fontSize: {
        'ss': '.666666rem',
      },
      spacing: {
        '14': '3.5rem',
        '18': '4.5rem',
      },
      margin: {
        '100': '33rem'
      },
      width: {
        '1/12': '8.333333%',
      },
      cubic: {
        'materialEaseIn': 'cubic-bezier(0.4, 0, 1, 1)',
        'materialEaseOut': 'cubic-bezier(0, 0, 0.2, 1)',
        'materialEaseInOut': 'cubic-bezier(0.4, 0, 0.2, 1)',
        'materialSharpEaseInOut': 'cubic-bezier(0.4, 0, 0.6, 1)'
      },
      stroke: {
        none: 'none'
      },
      cursor: {
        copy: 'copy'
      },
    }
  },
  variants: {
    // Some useful comment
  },
  plugins: [
    // Some useful comment
  ],
}
