import Vue from 'vue'
import TheHeader from '@/components/Blocks/TheHeader.vue'
import Wrapper from '@/components/Blocks/Wrapper.vue'
import Heading from '@/components/Blocks/Heading.vue'
import Paragraph from '@/components/Blocks/Paragraph.vue'
import EmptyTemplate from '@/components/Templates/EmptyTemplate.vue'
import AwesomeLandingTemplate from '@/components/Templates/AwesomeLandingTemplate.vue'

Vue.component('the-header', TheHeader)
Vue.component('wrapper', Wrapper)
Vue.component('heading', Heading)
Vue.component('paragraph', Paragraph)
Vue.component('empty-template', EmptyTemplate)
Vue.component('awesome-landing-template', AwesomeLandingTemplate)
