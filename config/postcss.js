import { join } from 'path'
import tailwind from 'tailwindcss'
import tc from '../tailwind.config.js'
import purgecss from '@fullhuman/postcss-purgecss'
import cssnano from 'cssnano'

class TailwindExtractor {
  static extract(content) {
    return content.match(/[A-Za-z0-9-_:\/]+/g) || [] // eslint-disable-line no-useless-escape
  }
}

const purgeAndNano = () => {
  if (process.env.NODE_ENV === 'production') {
    return [
      purgecss({
        content: [
          join(__dirname, '../pages/**/*.vue'),
          join(__dirname, '../layouts/**/*.vue'),
          join(__dirname, '../components/**/*.vue')
        ],
        extractors: [
          {
            extractor: TailwindExtractor,
            extensions: ['vue', 'js', 'html']
          }
        ],
        whitelist: ['html', 'body', 'nuxt-progress'],
        whitelistPatterns: [
          /-(enter|leave)(-(active|to))?$/
        ]
      }),
      cssnano({
        preset: 'default',
      })
    ]
  } else {
    return []
  }
}

export default {
  config: [
    tailwind(tc),
    require('autoprefixer')(),
    ...purgeAndNano()
  ]
}
