import Vue from 'vue'
import deepmerge from 'deepmerge'
import defaultConfig from 'tailwindcss/defaultConfig'
const theme = defaultConfig.theme

export const state = () => ({
  isEditor: false,
  templates: {
    'awesome-landing-template': {
      templateData: {
        wrapper: {
          styles: {
            bg: 'bg-indigo-600',
            width: 'w-full',
            height: 'h-screen'
          }
        },
        blocks: {
          'heading': {
            styles: {
              bg: 'bg-red-500',
              textColor: 'text-white',
              fontSize: 'text-5xl',
              fontWeight: 'font-bold'
            },
            content: 'This is awesome heading.'
          },
          'paragraph': {
            styles: {
              bg: 'bg-black',
              textColor: 'text-white',
            },
            content: 'This is awesome paragraph.'
          }
        }
      }
    },
    'empty-template': {}
  },
  activeTemplate: '',
  activeTemplateData: {
    wrapper: {
      styles: {}
    },
    blocks: {}
  },
  blocks: {
    'heading': {
      //
    },
    'paragraph': {
      //
    },
    'list': {
      //
    },
    'link': {
      //
    }
  }
})

export const getters = {
  bgStyles (state, getters, rootState) {
    let classes = []
    const colors = theme.colors
    Object.keys(colors).map(key => {
      if (typeof colors[key] === 'string') {
        classes.push(`bg-${key}`)
      } else {
        Object.keys(colors[key]).map(value => {
          classes.push(`bg-${key}-${value}`)
        })
      }
    })
    return classes
  },
  textColorStyles (state, getters, rootState) {
    let classes = []
    const colors = theme.colors
    Object.keys(colors).map(key => {
      if (typeof colors[key] === 'string') {
        classes.push(`text-${key}`)
      } else {
        Object.keys(colors[key]).map(value => {
          classes.push(`text-${key}-${value}`)
        })
      }
    })
    return classes
  },
  fontSizeStyles () {
    const classes = Object.keys(theme.fontSize).map(s => {
      return `font-${s}`
    })
    return classes
  },
  fontWeightStyles () {
    const classes = Object.keys(theme.fontWeight).map(s => {
      return `font-${s}`
    })
    return classes
  },
  heightStyles (state, getters, rootState) {
    const spacing = theme.spacing
    Object.assign(spacing, {
      auto: 'auto',
      full: '100%',
      screen: '100vh',
    })
    const height = Object.keys(spacing).map(s => {
      return `h-${s}`
    })
    return height
  },
  widthStyles (state, getters, rootState) {
    const spacing = theme.spacing
    Object.assign(spacing, {
      auto: 'auto',
      full: '100%',
      screen: '100vh',
      '1/2': '50%',
      '1/3': '33.33333%',
      '2/3': '66.66667%',
      '1/4': '25%',
      '3/4': '75%',
      '1/5': '20%',
      '2/5': '40%',
      '3/5': '60%',
      '4/5': '80%',
      '1/6': '16.66667%',
      '5/6': '83.33333%',
    })
    const width = Object.keys(spacing).map(s => {
      return `w-${s}`
    })
    return width
  }
}

export const mutations = {
  SET_ACTIVE_TEMPLATE (state, template) {
    state.activeTemplate = template
  },
  SET_CUSTOM_WRAPPER_STYLES (state, styles) {
    // Vue.set(state.activeTemplateData.wrapper, ...params)
    state.activeTemplateData.wrapper.styles = {...state.activeTemplateData.wrapper.styles, ...styles}
  },
  SET_CUSTOM_BLOCK_STYLES (state, block) {
    const merged = deepmerge(state.activeTemplateData.blocks, block)
    state.activeTemplateData.blocks = {...state.activeTemplateData.blocks, ...merged}
    // Vue.set(state.activeTemplateData.wrapper, ...params)
    // state.activeTemplateData.blocks[name].styles = {...state.activeTemplateData.blocks[name].styles, ...styles}
  },
  ADD_ACTIVE_BLOCK (state, block) {
    state.activeTemplateData.blocks = {...state.activeTemplateData.blocks, ...block}
  }
}
